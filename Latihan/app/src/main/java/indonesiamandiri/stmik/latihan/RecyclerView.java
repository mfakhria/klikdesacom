package indonesiamandiri.stmik.latihan;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

public class RecyclerView extends AppCompatActivity {

    private android.support.v7.widget.RecyclerView recyclerView;
    private android.support.v7.widget.RecyclerView.Adapter adapter;
    private List<list_item> ListItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);

        recyclerView = (android.support.v7.widget.RecyclerView) findViewById(R.id.RecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ListItem = new ArrayList<>();
        ListItem.add(new list_item("Asep Dardja", "Cicendo, Bandung", "3 menit lalu", "Abdi meni ganteng", R.drawable.ic_launcher_background));

    }
}
