package indonesiamandiri.stmik.latihan;

/**
 * Created by maula on 2/16/2018.
 */


public class adapter {

    private int imageUser;
    private String textNama, textAlamat, textWaktu, textStatus;

    public adapter(int imageUser, String textNama, String textAlamat, String textWaktu, String textStatus) {
        this.imageUser = imageUser;
        this.textNama = textNama;
        this.textAlamat = textAlamat;
        this.textWaktu = textWaktu;
        this.textStatus = textStatus;
    }

    public int getImageUser() {
        return imageUser;
    }

    public String getTextNama() {
        return textNama;
    }

    public String getTextAlamat() {
        return textAlamat;
    }

    public String getTextWaktu() {
        return textWaktu;
    }

    public String getTextStatus() {
        return textStatus;
    }

/*
    private List<list_item> listItems;
    private Context context;


    public adapter(List<list_item> listItems, Context context) {
        this.listItems = listItems;
    }


    @Override
    public adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(adapter.ViewHolder holder,int position) {
        list_item listItem = new list_item(position);

        holder.Nama.setText(listItem.getTextNama());
        holder.Alamat.setText(listItem.getTextAlamat());
        holder.Waktu.setText(listItem.getTextWaktu());
        holder.Status.setText(listItem.getTextStatus());
//        holder.Gambar.setImageDrawable(listItem.getImageUser());
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView Nama;
        public TextView Alamat;
        public TextView Status;
        public TextView Waktu;
  //      public ImageView Gambar;

        public ViewHolder(View itemView) {
            super(itemView);

            Nama    = (TextView) itemView.findViewById(R.id.textNameUser);
            Alamat  = (TextView) itemView.findViewById(R.id.textAlamat);
            Status  = (TextView) itemView.findViewById(R.id.textStatus);
            Waktu   = (TextView) itemView.findViewById(R.id.textWaktu);
 //           Gambar  = (ImageView) itemView.findViewById(R.id.ImageUser);

        }

*/

}
