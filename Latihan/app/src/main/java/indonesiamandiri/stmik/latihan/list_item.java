package indonesiamandiri.stmik.latihan;

/**
 * Created by maula on 2/16/2018.
 */

public class list_item {

    private String textNama, textAlamat, textWaktu, textStatus;
    private int imageUser;

    public list_item(String textNama, String textAlamat, String textWaktu, String textStatus, int imageUser) {
        this.textNama = textNama;
        this.textAlamat = textAlamat;
        this.textWaktu = textWaktu;
        this.textStatus = textStatus;
        this.imageUser = imageUser;
    }

    public String getTextNama() {
        return textNama;
    }

    public String getTextAlamat() {
        return textAlamat;
    }

    public String getTextWaktu() {
        return textWaktu;
    }

    public String getTextStatus() {
        return textStatus;
    }


    public int getImageUser() {
        return imageUser;
    }


}
