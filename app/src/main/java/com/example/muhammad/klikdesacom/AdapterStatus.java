package com.example.muhammad.klikdesacom;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by maula on 2/19/2018.
 */

class AdapterStatus extends RecyclerView.Adapter<AdapterStatus.ViewHolder> {
    private List<UserStatus> array_ViewMore;

    public AdapterStatus(List<UserStatus> array_ViewMore) {
        this.array_ViewMore = array_ViewMore;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_more_post_layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        UserStatus dateset = array_ViewMore.get(position);
        holder.imageUser.setImageResource(dateset.getImageUser());
        holder.userame.setText(dateset.getUsername());
        holder.tempat.setText(dateset.getTempat());
        holder.waktu.setText(dateset.getWaktu());
        holder.status.setText(dateset.getStatus());

    }

    @Override
    public int getItemCount() {
        return array_ViewMore.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageUser;
        public TextView userame, tempat, status, waktu;

        public ViewHolder(View itemView) {
            super(itemView);
            imageUser   = itemView.findViewById(R.id.ImageUser);

            userame     = itemView.findViewById(R.id.textNameUser);
            tempat      = itemView.findViewById(R.id.textAlamat);
            status      = itemView.findViewById(R.id.textStatus);
            waktu       = itemView.findViewById(R.id.textWaktu);

        }
    }
}
