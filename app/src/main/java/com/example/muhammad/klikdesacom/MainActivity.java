package com.example.muhammad.klikdesacom;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.Button;

import com.example.muhammad.klikdesacom.adapter.SnapAdapter;
import com.example.muhammad.klikdesacom.model.Snap;
import com.example.muhammad.klikdesacom.model.Stats;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements android.support.v7.widget.Toolbar.OnMenuItemClickListener {
    public static final String ORIENTATION = "orientation";
    private RecyclerView mRecyclerView;
    private boolean mHorizontal;
    private Button viewMore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);

        if (savedInstanceState == null) {
            mHorizontal = true;
        } else {
            mHorizontal = savedInstanceState.getBoolean(ORIENTATION);
        }

        setupAdapter();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(ORIENTATION, mHorizontal);
    }



    private void setupAdapter() {
        List<Stats> stats = getStats();
        List<Stats> narik = getMenarik();
        List<Stats> recent = getRecent();
        SnapAdapter snapAdapter = new SnapAdapter();
        snapAdapter.addSnap(new Snap("Berita",Gravity.CENTER_HORIZONTAL, "Recent Status", stats));

        snapAdapter.addSnap(new Snap("Berita",Gravity.CENTER_HORIZONTAL, "Berita Menarik", narik));
        snapAdapter.addSnap(new Snap("Berita",Gravity.CENTER_HORIZONTAL, "Berita Terbaru", recent));
        //snapAdapter.addSnap(new Snap(Gravity.CENTER_HORIZONTAL, "Recent Status", narik, Gravity.CENTER_HORIZONTAL, "Recent Status", news));

        mRecyclerView.setAdapter(snapAdapter);
    }

        private List<Stats> getStats() {
            List<Stats> stats = new ArrayList<>();
            stats.add(new Stats("Status","Muhammad Fakhri", R.drawable.ic_launcher_background));
            stats.add(new Stats("Status","Mr. Eri", R.drawable.ic_launcher_background));
            stats.add(new Stats("Status","Bapak AA", R.drawable.ic_launcher_background));
            stats.add(new Stats("Status","nama4", R.drawable.ic_launcher_background));
            stats.add(new Stats("Status","nama5", R.drawable.ic_launcher_background));
            stats.add(new Stats("Status","nama6", R.drawable.ic_launcher_background));
            stats.add(new Stats("Status","nama7", R.drawable.ic_launcher_background));
            stats.add(new Stats("Status","View More", 0));
            return stats;
        }

    private List<Stats> getRecent() {
        List<Stats> narik = new ArrayList<>();

        narik.add(new Stats("Berita","Baru Jadian besok langsung putus ", R.drawable.ic_launcher_background));
        narik.add(new Stats("Berita","Baru putus, langsung kangen mantan ", R.drawable.ic_launcher_background));
        narik.add(new Stats("Berita","Baru Baru aja kangen mantan, besoknya dapet undangan ", R.drawable.ic_launcher_background));

        return narik;
    }

    private List<Stats> getMenarik() {
        List<Stats> narik = new ArrayList<>();
        narik.add(new Stats("Berita","nama7", R.drawable.ic_launcher_background));
        narik.add(new Stats("Berita","nama7", R.drawable.ic_launcher_background));
        return narik;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.LayoutType) {
            mHorizontal = !mHorizontal;
            setupAdapter();
            item.setTitle(mHorizontal ? "Vertical" : "Horizontal");
        }
        return false;
    }
}
