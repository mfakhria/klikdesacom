package com.example.muhammad.klikdesacom.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.muhammad.klikdesacom.R;
import com.example.muhammad.klikdesacom.model.FeedNews;

import java.util.List;

/**
 * Created by muhammad on 15/02/18.
 */

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder> {

    private List<FeedNews> mDataset;
    public FeedAdapter(List<FeedNews> mDataset) {
        this.mDataset = mDataset;
    }



    @Override
    public FeedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.feed_item,parent,false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(FeedAdapter.ViewHolder holder, int position) {
        FeedNews dataset = mDataset.get(position);
        holder.mTime.setText(dataset.getTimeNewsFeed());
        holder.mImage.setImageResource(dataset.getNewsImagesFeed());
        holder.mTitle.setText(dataset.getTitleNewsFeed());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView mTitle;
        public TextView mTime;
        public ImageView mImage;

        public ViewHolder(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.news_title);
            mTime = itemView.findViewById(R.id.news_time);
            mImage = itemView.findViewById(R.id.news_image);

        }
    }
}