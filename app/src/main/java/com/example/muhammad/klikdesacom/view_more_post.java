package com.example.muhammad.klikdesacom;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class view_more_post extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter recyclerAdapter;
    private List<UserStatus> array;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_more_post);

        recyclerView = (RecyclerView) findViewById(R.id.RecyclerView_ViewMore_post);
        recyclerView.setHasFixedSize(true);

        array = new ArrayList<>();
        array.add(new UserStatus(R.drawable.ic_launcher_background, "Asep Dardja", "Cikoneng, Bandung", "Hay saya menggunakan aplikasi klik desa", "3 menit lalu"));
        array.add(new UserStatus(R.drawable.ic_launcher_background, "Tatang Somantri", "Cingised, Bandung", "Telah terjadi tauran antara kelompok pemuda, 2 bebek terluka dan propokator telah ditangkap untuk disembelih","5 menit lalu"));
        array.add(new UserStatus(R.drawable.ic_launcher_background, "Eneng Koswara","Parakan Saat, Bandung", "Kangge ibu-ibu nu tos di widian ku suamina manga ngiring arisan di rorompok abdi, nuhun", "1 jam lalu"));
        array.add(new UserStatus(R.drawable.ic_launcher_background, "Aceng Habib", "Ranca Kendal, Bandung", "Pangaosan di masjid Al-Hikmah bakalan di isi ku ustd Indra Maulana ti Rancaekek","1 jam lalu"));
        array.add(new UserStatus(R.drawable.ic_launcher_background, "Rangga Wicaksono", "Sentul, Bogor", "Hanya mengingatkan. Aplikasi ini dibuat untuk mencari desa, bukan mencari jodoh wkwk", "1 jam lalu"));

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerAdapter = new AdapterStatus(array);
        recyclerView.setAdapter(recyclerAdapter);

    }
}
